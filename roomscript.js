(async function () {
  console.log("Running Server...");
  const room = window.WLInit({
    token: window.WLTOKEN,
    roomName: "testing",
    maxPlayers: 12,
    public: false
  });
  window.WLROOM = room;

  room.setSettings({
    scoreLimit: 10,
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0
  });

  room.onRoomLink = (link) => console.log(link);
  room.onPlayerChat = (player, message) => console.log(`<${player.name}> ${message}`);
  room.onCaptcha = () => console.log("Invalid token");

  // ⚠️ Replace player auth with your own public key (the one shown here is pilaf's)
  const admins = new Set(["U2B9CG8CrqdrRwYGsRlv4SeuvjozuVatCa1YU3oiFtA"]);

  room.onPlayerJoin = (player) => {
      room.setPlayerAdmin(player.id, true);
  }
  console.log("yo", __getRandomName());
})();

window.WLROOM.onPlayerLeave = (player) => {
    console.log("onPlayerLeave playerteam",player.team, JSON.stringify(player), JSON.stringify(window.WLROOM.getPlayerScore(player.id)));
}

