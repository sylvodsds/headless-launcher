#!/bin/bash
chown -R pptruser:pptruser /home/pptruser

if [ $UID -eq 0 ]; then
  user="pptruser"
  dir=$2
  shift 2     # if you need some other parameters
  cd "$dir"
  exec su "$user" "$0" -- "$@"
  # nothing will be executed beyond that line,
  # because exec replaces running process with the new one
fi

export PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
#NPM_CONFIG_UNSAFE_PERM=true npm install -g .
npm i
