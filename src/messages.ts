
import {Metrics} from 'puppeteer';

export interface LaunchMsg {
    scriptPath: string | string[];
    token: string;
    id: string;
    hacked: boolean;
}

export interface RunScriptMsg {
    scriptPaths: string[];
    id: string;
}

export interface StopMsg {
    id: string;
}

export interface FollowMsg {
    id: string;
}

export interface MetricsMsg {
    id: string;
}

export interface MetricsResultMsg {
    /** The timestamp when the metrics sample was taken. */
    Timestamp: number;
    /** room id */
    Id: string;
    /** Combined duration of JavaScript execution. */
    ScriptDuration: number;
    /** Combined duration of all tasks performed by the browser. */
    TaskDuration: number;
    /** Used JavaScript heap size. */
    JSHeapUsedSize: number;
    /** Total JavaScript heap size. */
    JSHeapTotalSize: number;
}