import * as human from 'human-readable-id-gen';

export function getRandomName():string {
    let name=  human.generateId();
    return formatName(name);
}

export function formatName(name: string):string {
    return name.replace(/[\W_]+/g, "_");
}