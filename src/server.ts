#!/usr/bin/env node

import * as ipc from 'node-ipc';
import {Socket} from "net";
import * as fs from 'fs';
import * as path from 'path';
import * as puppeteer from 'puppeteer';
import { LaunchMsg, StopMsg, FollowMsg, MetricsMsg, MetricsResultMsg, RunScriptMsg } from './messages';
import {EventEmitter} from 'events';
import * as beauty from 'js-beautify';
import * as img from './images';
import * as names from './names';
import * as git from './git';
import * as dotenv from 'dotenv';
import * as process from 'process';
import { hackScript, getPaths } from './hack';
import {writeJS} from './files';

const args = [
	"--disable-background-networking",
	"--disable-background-timer-throttling",
	"--disable-client-side-phishing-detection",
	"--disable-extensions",
	"--disable-sync",
	"--disable-translate",
	"--allow-running-insecure-content",
    "--disable-web-security",
	"--no-sandbox",
	"--no-first-run",
	"--disable-features=WebRtcHideLocalIpsWithMdns"
]

const waitUntil = (condition: CallableFunction) => {
    return new Promise((resolve) => {
        let interval = setInterval(() => {
            if (!condition()) {
                return
            }

            clearInterval(interval)
            resolve()
        }, 100)
    })
}

const pages = new Map<string, RoomPage>();
class RoomPage extends EventEmitter {
	constructor(public page: puppeteer.Page) {
		super();

		page.on("console", async msg => {
			const args = await Promise.all(msg.args().map(arg => arg.executionContext().evaluate(arg => {
				if (arg instanceof Error) {
					return `${arg.name}: ${arg.message}`;
				}
				return arg;
			}, arg)));
			this.log(args.join(" "));
		});
	
		page.on("pageerror", (error) => {
			this.log(error.message);
		});
	
		page.on("error", (error) => {
			this.log(error.message);
		});
        
        page.exposeFunction('__getPaletteFromPng', img.getPaletteFromPng)
		page.exposeFunction('__convertPngToArray', img.convertPngToArray)
		page.exposeFunction('__getRandomName', names.getRandomName)
        page.exposeFunction('__commitLevel', git.commitLevel)
        page.exposeFunction('__getInterestingPaths', getPaths)
	}

	async setToken(token:string) {
		await this.page.evaluate((token) => (window as any).WLTOKEN = token, token);
	}

	async loadHeadless(script: string, hacked: boolean) {
		this.log("Loading headless...");
        if (hacked) {
            const hackedScriptContents = await fs.promises.readFile(path.resolve(script), "utf8");
            this.log(`hacked script length ${hackedScriptContents.length}`)

            await this.page.setRequestInterception(true);

            this.page.on("request", interceptedRequest => {
            const url = interceptedRequest.url();

                if (url.indexOf("headless-min.js")>-1) {
                    interceptedRequest.respond({
                    body: hackedScriptContents
                    });
                } else {
                    interceptedRequest.continue();
                }
            });
        }
		
		await this.page.goto("https://www.webliero.com/headless");
		await this.page.evaluate( function() {
			if ( (window as any).WLInit != null ) {
				return Promise.resolve();
			}
			return new Promise((resolve, reject) => {
				(window as any).onWLLoaded = () => resolve();
			});
		});
		this.log("Loaded headless");
		await this.page.evaluate( function() {
			if ( (window as any).EVIL != null ) {
					console.log("hacked");
				}
			return Promise.resolve();
		});
	}

	async runScriptPath(scriptPath:string) {
		this.log(`Loading script ${scriptPath}`);
		const scriptFileContents = await fs.promises.readFile(path.resolve(scriptPath), "utf8");
		await this.page.evaluate(scriptFileContents);
		/*
		await this.page.evaluate(`(async function() { try { ${scriptFileContents}
			} catch(e) {
				console.log(e);
				throw new Error("Failed to run script.");
			}
		})();`);
		*/
	}

	log(msg:string) {
		this.emit('log', msg);
	}
}

async function createRoomPage(browser: puppeteer.Browser, id:string) {
	if ( pages.get(id) != null ) throw Error(`Cannot create room page '${id}' because it already exists.`);
	const roomPage = new RoomPage(await browser.newPage());
	pages.set(id, roomPage);
	roomPage.on('log', (msg) => console.log(`"${id}": `, msg));
	return roomPage;
}

export async function startServer(show:boolean, chromeExecPath? : string) {
	dotenv.config();
	
	const chromeExecutablePath   = process.env.CHROME_EXECPATH ?? chromeExecPath;
	const gitlabBuilderMaptoken  = process.env.GITLAB_BUILDER_TOKEN ?? '';
    const gitlabBuilderMapsLocal = process.env.GITLAB_BUILDER_MAPS_LOCAL ?? '';
    const gitlabUserMail         = process.env.GITLAB_USER_MAIL ?? '';
    let   headlessScript         = process.env.HEADLESS_SCRIPT ?? 'headless-min.js';

    if (gitlabBuilderMaptoken!=""){
        git.initConfig(gitlabBuilderMaptoken,gitlabBuilderMapsLocal, gitlabUserMail)
    }
	console.log("Starting chromium...", chromeExecutablePath, gitlabBuilderMaptoken, gitlabBuilderMapsLocal);
	const browser = await puppeteer.launch({headless: !show, args, executablePath: chromeExecutablePath});
	console.log("Chromium up");

    if (gitlabBuilderMaptoken!=""){
        await git.pull();
    }
	ipc.config.id = 'wlserver';
	ipc.config.silent = true;
	ipc.config.maxRetries = 0;

	ipc.serve(function() {
		const srv = ipc.server;

		function followPage(socket:Socket, roomPage:RoomPage) {
			function stop() {
				roomPage.off('log', connectionLog);
				socket.destroy();
			}
			function connectionLog(msg:string) {
				srv.emit(socket, 'message', msg);
			}
			roomPage.on('log', connectionLog);
			socket.on('close', stop);
			return stop;
		}


		async function fetchMofo(page: puppeteer.Page, socket:Socket) {
            let done =false;
            await page.setRequestInterception(true);
			page.on('requestfinished', (request) => {
				if (request.url().indexOf("headless-min.js")>-1) {
					srv.emit(socket, 'message', request.url());
					var response = request.response();
					
					try {
						//headless-min.js
						if (request.redirectChain().length === 0) {
							if (response!==null) {
								response.buffer().then(responseBody => {
                                    const body = beauty.js_beautify(responseBody.toString(),{
                                        "indent_size": 4,
                                        "indent_char": " ",
                                        "max_preserve_newlines": 5,
                                        "preserve_newlines": true,
                                        "keep_array_indentation": false,
                                        "break_chained_methods": false,
                                        "brace_style": "collapse",
                                        "space_before_conditional": true,
                                        "unescape_strings": false,
                                        "jslint_happy": false,
                                        "end_with_newline": false,
                                        "wrap_line_length": 0,
                                        "comma_first": false,
                                        "e4x": false,
                                        "indent_empty_lines": false
                                      })
                                    const ts =  Math.round(new Date().getTime()/1000)
                                    writeJS(`headless-min-original-${ts}.js`, body)
                                    srv.emit(socket, 'message', "original file written")
                                    try {
                                        console.log("trying to hack")
                                        srv.emit(socket, 'message', "try hacking script")
                                        let hack = hackScript(body)
                                        srv.emit(socket, 'message', "matches "+JSON.stringify([...hack.matches]))
                                        srv.emit(socket, 'message', "paths "+JSON.stringify([...hack.paths]))
                                        writeJS("headless-min.js", hack.script)
                                    } catch (err) {                                     
                                        srv.emit(socket, 'message', "error hacking script")
                                        srv.emit(socket, 'message', err.toString())
                                
                                    }
                                });										
							}
							
						}
					} catch (err) { 
                        srv.emit(socket, 'message', "error fetching script");
                        srv.emit(socket, 'message', err.toString());
                    }
                }	
                done = true;			
			});
			page.on('request', request => {
                request.continue();            
			});	
            await page.goto("https://www.webliero.com/headless");	   
            await waitUntil(() => done==true)  // yes I hate promises .. just waiting for the socket to recieve messages
		}
		srv.on('error', function(err) {
			console.log(err);
		});

		srv.on('fetch-script', async function(data: LaunchMsg,socket:Socket) {
			try {
				const page = await browser.newPage();
				srv.emit(socket, 'message', "Fetching headless script...");
				
				await fetchMofo(page, socket);
                console.log("already fetched")
				srv.emit(socket, 'message', "fetched");
			}catch(e) {
				srv.emit(socket, 'message', e.toString());
			}
			socket.destroy();
		});

		srv.on('launch', async function(data: LaunchMsg,socket:Socket) {
			const {id, scriptPath, token, hacked} = data;
			const scripts = typeof scriptPath == "string"? [scriptPath]: scriptPath;
			if ( pages.get(id) != null ) {
				srv.emit(socket, 'message', `"${id}" is already running, you must stop it first.`);
				socket.end();
				return;
			}
			srv.emit(socket, 'message', `Launching room id="${id}" token="${token}"`);

			try {
				const roomPage = await createRoomPage(browser, id);
				followPage(socket, roomPage);
				await roomPage.loadHeadless(headlessScript, hacked);				
				await roomPage.setToken(token);
				for(let script of scripts) {
					await roomPage.runScriptPath(script);
				}
			}catch(e) {
				srv.emit(socket, 'message', e.toString());
			}
		});

		srv.on('run-script', async function(data: RunScriptMsg, socket: Socket) {
			const {id, scriptPaths} = data;
			const roomPage = pages.get(data.id);
			if ( roomPage == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			const stop = followPage(socket, roomPage);
			try {
				for(let script of scriptPaths) {
					await roomPage.runScriptPath(script);
				}
			}catch(e) {
				console.log(e.toString());
			}
			finally {
				// Wait a bit to allow console message events produced by the script to arrive before closing the cli connection.
				await new Promise(resolve => setTimeout(resolve, 50));
				stop();
			}
		});

		srv.on(
			'ls',
			function(data,socket:Socket){
				console.log("ls");
				for( let key of pages.keys() ) {
					srv.emit(socket, 'message', key);
				}
				socket.end();
			}
		);

		srv.on('stop', async function(data: StopMsg, socket:Socket) {
			const roomPage = pages.get(data.id);
			if ( roomPage == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			pages.delete(data.id);
			if ( roomPage.page != null ) {
				await roomPage.page.goto("about:blank");
				await roomPage.page.close();
			}
			srv.emit(socket, 'message', `"${data.id}" closed`);
			socket.end();
		});

		srv.on('follow', async function(data: FollowMsg, socket:Socket) {
			const roomPage = pages.get(data.id);
			if ( roomPage == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			srv.emit(socket, 'message', `Following room id="${data.id}"`);

			followPage(socket, roomPage);
		});

		srv.on('metrics', async function(data: MetricsMsg, socket:Socket) {
			const roomPage = pages.get(data.id);
			if ( roomPage == null || roomPage.page == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			const page = roomPage.page;
			let done = false;
			socket.on('close', () => done=true);

			async function sendMetrics() {
				if ( done ) return;
				const metrics = await page.metrics();
				srv.emit(socket, 'metrics', {
					Id: data.id,
					Timestamp: metrics.Timestamp,
					JSHeapTotalSize: metrics.JSHeapTotalSize,
					JSHeapUsedSize: metrics.JSHeapUsedSize,
					ScriptDuration: metrics.ScriptDuration,
					TaskDuration: metrics.TaskDuration
				} as MetricsResultMsg);
				setTimeout(sendMetrics, 1000);
			}
			sendMetrics();
		});
	});

	ipc.server.start();

	browser.on('disconnected', function() {
		ipc.server.stop();
	});
}
