import * as beauty from 'js-beautify';

let h:Hack= {
    script:'',
    paths: new Map<string,string>(),
    matches: new Map<string,string>(),
    extendedApiFuncs: new Array<string>(),
    eventCallbacks: new Map<string,string>(),
}
const EVENT_CALLBACKS_PLACEHOLDER='#EVENT_CALLBACKS_PLACEHOLDER#'

export interface Hack {
    script: string;
    paths: Map<string,string>; // property names
    matches:Map<string,string>; // class names etc
    extendedApiFuncs: Array<string>
    eventCallbacks: Map<string,string>
}
function addMatch(k:string, v:string) {
    console.log("MATCH:", k,v);
    h.matches.set(k, v)
    
}
function addPath(k:string, v:string) {
    console.log("PATH:", k,v);
    h.paths.set(k, v)
    
}
export function getPaths():Object {
    return Object.fromEntries(h.paths)
}
export function hackScript(script: string):Hack {
    h.script = script
    
    addHackSuccess()
    addRoomObjectReference()
    addReadPngRef()
    extendGametickAndAddPlaceholder()

    // resolves paths to get the wObjects by its start frame (might be a weak implementation as start frame could be shared by multiple objects)
    resolveOnWObjectFromStartFramePathMatches()

    // --more player data https://www.vgm-quiz.com/dev/webliero/headless-player
    extendPlayerData()
    //-------todo  https://www.vgm-quiz.com/dev/webliero/headless-onplayerhit
   // addOnPlayerHit()

    // adds new extended api functions

    //addAPIWeaponBans()
    //addAPIGetWeapon()
 
    insertExtendedAPI()
    insertNewEventCallbacks()

    h.script = beauty.js_beautify(h.script.toString(),{
        "indent_size": 4,
        "indent_char": " ",
        "max_preserve_newlines": 5,
        "preserve_newlines": true,
        "keep_array_indentation": false,
        "break_chained_methods": false,
        "brace_style": "collapse",
        "space_before_conditional": true,
        "unescape_strings": false,
        "jslint_happy": false,
        "end_with_newline": false,
        "wrap_line_length": 0,
        "comma_first": false,
        "e4x": false,
        "indent_empty_lines": false
      })
    return h;
}

// adds EVIL=true at the start of script
function addHackSuccess() {
    const o = h.script
    h.script = h.script.replace(/(strict';\n\(function\(\w{2}\)\s{\n)/g, "$1    window.EVIL=true;\n");
    if (o==h.script) {
        throw Error('hack success failed')
    }
}


// keeps a reference of the room in window.REF_ROOM_STATE 
//           if (null != v && (A = ha.th(v), 3 < A.Xb.length)) throw w.A("Invalid country code");
// let t = new ba;
// window.REF_ROOM_STATE=t;
function addRoomObjectReference() {
    const o = h.script
    let found = Array.from(h.script.matchAll(/\w\("Invalid country code"\);\s+let\s(\w)\s=\snew\s(\w{2});\n\s+\w\.(\w)/g))[0]
    if (found.length!=4) {
        throw Error('addRoomObjectReference hack failed')
    }
    addMatch("API_VAR", found[1])  //"t"
    addMatch("ROOM_CLASS", found[2])  //"ba"
    addPath("GAME_STATE",found[3])//"D"

    h.script = h.script.replace(/(\w\("Invalid country code"\);\s+let\s)(\w)(\s=\snew\s\w{2};)/g,"$1$2$3\n            window.REF_ROOM_STATE=$2;\n");
    if (o==h.script) {
        throw Error('ref room state failed')
    }
}

// keeps a reference of the method used for reading pngs in window.__ReadPNG 
//    db.c = !0;
// window.__ReadPNG = db.read; // global link of the png read func for room level usage
// class O {
function addReadPngRef() {
    const o = h.script
    let found = Array.from(h.script.matchAll(/(\w{2})\.\w\s=\s!0;\s+class\s\w\s{\s+static\s\w{2}\(\w\)/g))[0]
    if (found.length!=2) {
        throw Error('addReadPngRef hack failed')
    }
    addMatch("READER", found[1])  //"db"

    h.script = h.script.replace(/((\w{2})\.\w\s=\s!0;)(\s+class\s\w\s{\s+static\s\w{2}\(\w\))/g,"$1\n  window.__ReadPNG = $2.read;\n$3");
    if (o==h.script) {
        throw Error('ref read png failed')
    }
}



// expands gametick to pass the game state to it && add a place holder for next event inserts
// credits: OPHI
// ------------
// t.uh = function() { <- t = parento, uh = func
//     let m = G.onGameTick;
//     null != m && m() <- m ="infunc"
// };
// here we want to pass a new param (gameparam) to function & m() and keep match for "uh" func
// ------------------
// null != this.uh && this.uh() <- here we want to pass "this" to "uh"
//
// we want to decipher the game's states many interesting path eg gamestate.D.m.K[weaponID].name
//
function extendGametickAndAddPlaceholder() {
    const o = h.script
    // finds parameters
    let found = Array.from(h.script.matchAll(/(\w{1})\.(\w{2})\s=\sfunction\(\)\s{\n\s+let\s(\w)\s=\s(\w)\.onGameTick;\n/g))[0]
    if (found.length!=5) {
        throw Error('gametick extension hack failed')
    }
    const parento = found[1] // t
    const func = found[2]  // uh
    const infunc = found[3] // m
    const apiobject = found[4] // G
    addMatch("API_OBJ", apiobject)
    const gameparam = "game"
    // first replace
    // infunc match unaware pattern
    // }\n\s+null\s\!=\sthis\.\w{2}\s&&\sthis\.\w{2}\(\)\n\s+} 
    // infunc aware pattern:
    let c = new RegExp(`(\\}\\n\\s+null\\s\\!=\\sthis\\.${infunc}\\s&&\\sthis\\.${infunc}\\()(\\)\\n\\s+\\})`)
    h.script = h.script.replace(c,"$1this$2")
    // second replace
    // unique enough not to care about naming of variables
    h.script = h.script.replace(/(\w{1}\.\w{2}\s=\sfunction\()(\)\s{\n\s+let\s\w\s=\s\w\.onGameTick;\n\s+null\s\!=\s\w\s&&\s\w\()(\)\n\s+};\n)/g, "$1"+gameparam+"$2"+gameparam+"$3"+'\n'+EVENT_CALLBACKS_PLACEHOLDER)
   
    if (o==h.script) {
        throw Error('gametick extension failed')
    }
}

//
//https://www.vgm-quiz.com/dev/webliero/headless-player
// ----------------
// function g(m) {
// 	return null == m ? null : {
// 		worm:m, // we add the reference at first
// 		name: m.Ra,
// 		team: m.ra,
// 		id: m.ga,
// 		admin: m.Oc
// 	}
// }
// todo map player class property names:
//  class lb {
// constructor() {
//     this.ie = [0,
//         1, 2, 3, 4
//     ];
//     let a = [],
//         b = 0;
//     for (; 5 > b;) ++b, a.push(new kb);
//     this.wc = a;
//     this.Ef = this.ga = this.ra = this.Mf = 0;
//     this.Ra = "Player";
//     this.ae = 0;
//     this.Sc = null;
//     this.Oc = this.yf = !1;
//     this.Rc = 16711680;
//     this.sa = null
// }
function extendPlayerData() {
    const o = h.script
    h.script = h.script.replace(/(function\s\w\((\w)\)\s{\n\s+return\snull\s==\s\w\s\?\snull\s:\s{\n)/g, "$1worm:$2,\n")
    if (o==h.script) {
        throw Error('player data extension failed')
    }
}


// in class ba "D" is roomstate
//  class ba {
// constructor() {
//     this.Xd = 4;
//     this.$d = this.Ud = !1;
//     this.jb = new Map;
//     this.rc = 5;
//     this.Nd = !1;
//     this.Eb = 4;
//     this.Pd = 0;
//     this.Tc = !1;
//     this.Qa = new Ba;
//     this.D = new qa;
//
//  is "list[i].type[Oa]" ??
// c = this.Bb;
// b = c.list;
// c = c.P;
// for (d = 0; d < c;) b[d++].l(a);
//
//

function resolveOnWObjectFromStartFramePathMatches() {
        // find class da (contains list & P (count))
        // -----------
        // class da {
        //     constructor(a, b) { b is "new nb"
        //         var c = [];
        //         let d = 0;
        //         for (; d < a;) ++d, c.push(b());
        //         this.Ie =
        //             c;
        //         this.Je = a;
        //         b = [];
        //         for (c = 0; c < a;) b.push(this.Ie[c++]);
        //         this.list = b;
        //         this.P = 0
        //     }
        //
        let listAndObjCount = Array.from(h.script.matchAll(/class\s+(\w{2})\s+\{\n\s+constructor\(\w\,\s\w\)\s+\{\n\s+var\s\w\s=\s\[\];\n(.|\n)*\n\s+this\.list\s=\s\w;\n\s+this\.(\w)\s=\s\d\n\s+\}/g))[0]
        if (listAndObjCount.length!=4) {
            throw Error('addOnWObjectFromStartFramePathMatches failed on listAndObjCount')
        }
        const objListClass = listAndObjCount[1];

        addMatch("OBJ_LIST_CLASS", objListClass) // "da"

        addPath("OBJ_COUNT", listAndObjCount[3]) // "P"
   

        // find Bb (in qa) (variable instanciate da)
        // -----------
        // this.Bb = new da(1E3, function() {
        //     return new nb
        // });
        // aware of "da" but not "nb" (should probably be)
        //  let list = Array.from(h.script.matchAll(/this\.(\w{2})\s=\snew\sda\(1E3\,\sfunction\(\)\s\{\n\s+return\snew\s\w{2}\n\s+\}\);\n\s+this\.\w{2}\s=\s\[\];/g))[0]
        let lc = new RegExp(`this\\.(\\w{2})\\s=\\snew\\s${objListClass}\\(1E3\\,\\sfunction\\(\\)\\s\\{\\n\\s+return\\snew\\s\\w{2}\\n\\s+\\}\\);\\n\\s+this\\.\\w{2}\\s=\\s\\[\\];`,'g')
        let list = Array.from(h.script.matchAll(lc))[0]
        if (list.length!=2) {
            throw Error('addOnWObjectFromStartFramePathMatches failed on OBJ_LIST')
        }

        addPath("OBJ_LIST", list[1]) // "Bb"
        // we want to find the property matching "startFrame" on wObjects (repeat & objTrailType are specific to wObjects)
        // static cm(a) {
        //     let b = new Ja;
        // [..]
        //     b.Lb = a.bloodOnHit;
        //     b.Oa = a.startFrame;
        //     b.ea = a.numFrames;
        //     b.Ee = a.loopAnim;
        //     b.wb = a.shotType;
        //     b.repeat = a.repeat;
        //     b.$b = a.colorBullets;
        //     b.Rb = a.splinterAmount;
        //     b.Sb = a.splinterColour;
        //     b.Tb = a.splinterType;
        //     b.Ug = a.splinterScatter;
        //     b.He = a.objTrailType;
        //     b.Gg = a.objTrailDelay;
        //     b.Ig = a.partTrailType;
        //     b.qd = a.partTrailObj;
        //     b.Hg = a.partTrailDelay;
        //     b.speed = a.speed;
        //     return b
        // }
        // static dm(a) {
        let startFrame = Array.from(h.script.matchAll(/\w\.(\w{2})\s=\s\w\.startFrame\;\n(\s+\w\.\w{2}\s=\s\w\.\w*\;\n)+\s+\w\.repeat\s=\s\w\.repeat/g))[0]
        if (startFrame.length!=3) {
            throw Error('addOnWObjectFromStartFramePathMatches failed on startFrame')
        }

        addPath("WOBJ_STARTFRAME", startFrame[1]) // "Oa"
    }
// easy to find placeholder for extended api after:
// getTeamScore: function(m) {
//     return t.Qa.rg(m)
// }
function insertExtendedAPI() {
    const o = h.script
    const funcs = h.extendedApiFuncs.join(',\n')
    h.script = h.script.replace(/(getTeamScore:\sfunction\(\w\)\s{\n.*\n.*})/g, "$1,\n"+funcs)
   
    if (o==h.script) {
        throw Error('inserting extended api failed')
    }

}
// adds API methods to ban & unban weapons
// credits: OPHI
// -----------------
// getTeamScore: function(m) {
//     return t.Qa.rg(m)
// },
// unbanAllWeapons:function(){
//     t.jb.clear();
//     let c = new Ta; // create weapon ban message
//     c.vd = -1; // all weapons
//     c.newValue = 0; // unban=0 ban=1
//     e(c);// send message for sync
// },
// banWeapon:function(name,ban){
//     var W = t.D.m.K; // weapon list
//     for(let i=0;i<W.length;i++){
//         if(W[i].name==name){
//             if(ban) t.jb.set(i,1);
//             else t.jb.delete(i);    
//             let c = new Ta; // craft message
//             c.vd = i;
//             c.newValue = ban?1:0;
//             e(c); // send
//             break;
//         }
//     }
// }
function addAPIWeaponBans() {
    const av = h.matches.get("API_VAR") // "t"
    const gs = h.paths.get("GAME_STATE") // "D"
    // TODO: needs to make this dynamic
    // requires paths:
    // t.jb        ? weapon ban list?
    // vd          property in Ta for weapon ID
    // t.D.m.K     weapon list
    //class name
    // Ta          weapon ban message
    // func name
    // e           event dispatch
    //
    h.extendedApiFuncs.push(`
        unbanAllWeapons:function(){
            ${av}.jb.clear();
            let c = new Ta; // create weapon ban message
            c.vd = -1; // all weapons
            c.newValue = 0; // unban=0 ban=1
            e(c);// send message for sync
        }
    `)
    h.extendedApiFuncs.push(`
        banWeapon:function(name,ban){
            var W = ${av}.${gs}.m.K; // weapon list
            for(let i=0;i<W.length;i++){
                if(W[i].name==name){
                    if(ban) ${av}.jb.set(i,1);
                    else ${av}.jb.delete(i);    
                    let c = new Ta; // craft message
                    c.vd = i;
                    c.newValue = ban?1:0;
                    e(c); // send
                    break;
                }
            }
        }
   `)

}

// adds an api fun to get weapon info from id
function addAPIGetWeapon() {
    const av = h.matches.get("API_VAR") // t
    const gs = h.paths.get("GAME_STATE") // D
    // TODO: needs to make this dynamic
    // requires paths:
    // t.jb        ? weapon ban list?
    // vd          property in Ta for weapon ID
    // t.D.m.K     weapon list)
    h.extendedApiFuncs.push(`getWeapon:(id) => ${av}.${gs}.m.K[id]`)


}

//
// https://www.vgm-quiz.com/dev/webliero/headless-onplayerhit
//
//
// add `this.onPlayerHit = null;` to qa
// class qa {
//     constructor() {
// this.Ac = 2;
// this.pe = 1800;
// this.Se = 0;
//----------------
// add `
// let damage = Math.min(this.va, this.va - e);
//         if (damage > 0) {
//             Jb.fa(a.onPlayerHit, this.C, c, damage, d);
//         }`
// to qb
// =>
//    // worm.updateHit(game, damage, shooterID, weaponID)
//    qb(a, b, c, d) {
//     // if not dead
//     if (0 < this.va) {
//         // reduce health
//         let e = this.va - b * a.te;  // a.te = damage multiplier (room setting)
//         // call new event handler
//         let damage = Math.min(this.va, this.va - e);
//         if (damage > 0) {
//             Jb.fa(a.onPlayerHit, this.C, c, damage, d);
//         }
//         // bound health to [0, 100] range
//         this.va = 100 < e ? 100 : 0 > e ? 0 : e;
//         // if player was killed, call event handler
//         0 >= this.va ? (
//--------------------
// add
// `          this.D.onPlayerHit = function(hurtID, shooterID, damage, weaponID) {
//     const hurt = a.F.get(hurtID);
//     const shooter = a.F.get(shooterID);
//     if (hurt != null && shooter != null) {
//         a.onPlayerHit(hurt, shooter, damage, weaponID);
//     }
// }`
// to class ba after 
// this.D.xi = function(b, c, d) {  // b=deadID, c=killerID, d=weaponID
//     b = a.F.get(b);
//     null != b && (c = a.F.get(c), a.Qa.Ng(a, b, c), Ib.fa(a.Yj, b, c, d))
// }
function addOnPlayerHit() {
    let o = h.script
    const ophnull = `this.onPlayerHit = null;\n`

    h.script = h.script.replace(/(class\s\w+\s{\n\s+constructor\(\)\s{\n\s+this\.\w{2}\s=\s2;\n)/g, "$1"+ophnull)
    if (o==h.script) {
        throw Error('adding on player hit failed '+ophnull)
    }
    /*--------------------*/
    //todo: make this dynamic
    o=h.script

    const damage = `
    let damage = Math.min(this.va, this.va - e);
            if (damage > 0) {
                Jb.fa(a.onPlayerHit, this.C, c, damage, d);
            }\n`
    h.script = h.script.replace(/(\w{2}\((\w,\s){3}\w\)\s{\n\s+if\s\(0\s<\sthis\.\w{2}\)\s{(\s+.+;\n){1})/g, "$1"+damage)
    if (o==h.script) {
        throw Error('adding on player hit failed (damage)')
    }
    /*--------------------*/
    //todo: make this dynamic
    o = h.script
    const ophfunc =`\n          this.D.onPlayerHit = function(hurtID, shooterID, damage, weaponID) {
            const hurt = a.F.get(hurtID);
            const shooter = a.F.get(shooterID);
            if (hurt != null && shooter != null) {
                a.onPlayerHit(hurt, shooter, damage, weaponID);
            }
        }\n`
    h.script = h.script.replace(/(null\s\!=\s\w\s&&\s\(\w\s=\s\w\.\w\.get.*\n\s+}\n)/g, "$1"+ophfunc)
    if (o==h.script) {
        throw Error('adding on player hit failed (onplayerhitfunc)')
    }
    h.eventCallbacks.set("onPlayerHit",`t.onPlayerHit = function (m, u, damage, weaponID) {
             let z = G.onPlayerHit;
             null != z && z(g(m), g(u), damage, weaponID)
         }`)
}
//--------------------
// finally adds listener to "t"
// t.onPlayerHit = function (m, u, damage, weaponID) {
//     let z = G.onPlayerHit;
//     null != z && z(g(m), g(u), damage, weaponID)
// };
// for ex next to:
// t.uh = function() {
//     let m = G.onGameTick;
//     null != m && m()
// };
function insertNewEventCallbacks() {
    const o = h.script
    const funcs = Array.from(h.eventCallbacks.values()).join(';\n')
    h.script = h.script.replace(EVENT_CALLBACKS_PLACEHOLDER, "\n/*---custom  callbacks-----*/\n"+funcs+";\n/*------------------*/\n")
   
    if (o==h.script) {
        throw Error('inserting extended api callbacks failed')
    }

}

// function getRandomCharNotMatchin(cs: string[]):string {
//     let c = getRandomChar()
//     while (cs.includes(c)) {
//         c = getRandomChar()
//     }
//     return c
// }

// function getRandomChar():string {
//     const possible = "abcdefghijklmnopqrstuvwxyz"
//     return possible.charAt(Math.floor(Math.random() * possible.length))
// }