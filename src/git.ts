import * as img from './images';
import * as names from './names';
import * as fs from 'fs';
import * as nodegit from 'nodegit';
import * as path from 'path';

export var conf: Config;

export interface Config {
    builderLocalPath: string;
    builderToken: string;
    builderRepo:string;
    gitlabUserMail: string;
}
function getSig():nodegit.Signature {
    return nodegit.Signature.now("dsds dsds", conf.gitlabUserMail)
}

export function initConfig(builderToken:string, builderLocalPath:string, gitlabUserMail:string) {
    conf = {
        builderToken:builderToken,
        builderLocalPath: builderLocalPath,
        builderRepo:'https://gitlab.com/sylvodsds/webliero-builder-maps.git',
        gitlabUserMail: gitlabUserMail
    }
}

export async function commitLevel(playername: string, name: string, data: Array<number>, width: number, height: number):Promise<string> {
    if (name == null || name=="") {
        name = names.getRandomName();
    }
    await pull();

    name = fixUniquePngName(name);
    let lev = {
        name: name,
        width: width,
        height: height,
        data: data,
        palette: []
    }

    await img.createPngFromLev(conf.builderLocalPath+'/maps/'+name, lev);
    const repo = await nodegit.Repository.open(conf.builderLocalPath)
    
    const index = await repo.refreshIndex();
  
    // this file is in the root of the directory and doesn't need a full path
    await index.addByPath('maps/'+name);
    // this will write both files to the index
    await index.write();
    const oid = await index.writeTree();

    const parent = await repo.getHeadCommit();

    const commitId = await repo.createCommit("HEAD", getSig(), getSig(), `adds ${name} by ${playername}`, oid, [parent]);
    const remote = await repo.getRemote("origin")

    console.log("New Commit: ", commitId);

    await remote.push(['refs/heads/master:refs/heads/master'],
    {
        callbacks: {
            credentials: () => nodegit.Cred.userpassPlaintextNew(conf.builderToken,conf.builderToken),
            certificateCheck: () => 0
        }
        }
    );
    
    return name
}

function fixUniquePngName(name:string):string {
    name = name.replace('.png','');
    while (fs.existsSync(conf.builderLocalPath+'/maps/'+name+'.png')) {
        name = name+'_'+Math.round(Math.random()*100);
    }
    return name+'.png';
}

export async function pull() {
    if (!fs.existsSync(conf.builderLocalPath)) {
        throw Error('repository path does not exist');
    }
    if (!fs.existsSync(conf.builderLocalPath+'/maps/')) {
        await clone();
    }
    let repo = await nodegit.Repository.open(conf.builderLocalPath)

    await repo.fetchAll({
        callbacks: {
            credentials: () => nodegit.Cred.userpassPlaintextNew(conf.builderToken,conf.builderToken),
            certificateCheck: () => 0
        }
        })
    await repo.mergeBranches("master", "origin/master")
}


async function clone() {
    if (!fs.existsSync(conf.builderLocalPath)) {
        throw Error('repository path does not exist');
    }

    let repo = await nodegit.Clone.clone(conf.builderRepo, conf.builderLocalPath,
        {
            fetchOpts: {
              callbacks: {
                credentials: () => nodegit.Cred.userpassPlaintextNew(conf.builderToken,conf.builderToken),
                certificateCheck: () => 0
              }}}
        )
}
