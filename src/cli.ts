#!/usr/bin/env node

import * as ipc from 'node-ipc';
import {program} from 'commander'
import * as path from 'path';
import { LaunchMsg, StopMsg, MetricsMsg, MetricsResultMsg, RunScriptMsg } from './messages';
import { startServer } from './server';
import * as prettyBytes from 'pretty-bytes';

function prepareScreenForAnimation() {
	process.stdout.write('\n'.repeat(process.stdout.rows));
}

function connect(messageType: string, data: any) {
	ipc.config.id   = 'wlcli';
	ipc.config.maxRetries = 0;
	ipc.config.silent = true;

	ipc.connectTo(
		'wlserver',
		function() {
			const srv = ipc.of.wlserver;

			srv.on('connect', function() {
				srv.emit(messageType, data);
			});

			srv.on('disconnect', function() {});

			srv.on('error', function(err) {
				console.log("Connection Error");
			});

			srv.on('message', function(msg:any) {
				console.log(msg);
			});

			let lastMetrics: MetricsResultMsg | null = null;
			srv.on('metrics', function(msg:MetricsResultMsg) {
				const prev = lastMetrics;
				lastMetrics = msg;

				let scriptPercent = 0;
				let taskPercent = 0;

				if ( prev != null ) {
					const interval = msg.Timestamp - prev.Timestamp;
					scriptPercent = 100 * (msg.ScriptDuration - prev.ScriptDuration) / interval;
					taskPercent = 100 * (msg.ScriptDuration - prev.ScriptDuration) / interval;
				}

				const out = process.stdout;
				out.cursorTo(0, 0);
				out.clearScreenDown();
				out.write(`Stats for '${msg.Id}':\n`);
				out.write(`Heap: ${prettyBytes(msg.JSHeapUsedSize)} / ${prettyBytes(msg.JSHeapTotalSize)}\n`);
				out.write(`Scripts %: ${taskPercent.toFixed(2)}\n`);
				out.write(`Task %: ${taskPercent.toFixed(2)}\n`);
			});
		}
	);
}

function launch(scripts:string[]) {
	const opts = launchCommand.opts();
	const message: LaunchMsg = {
		scriptPath: scripts.map((scriptPath) => { return path.resolve(scriptPath) }),
		token: opts.token,
        id: opts.id,
        hacked: !opts.nohack
	}

	connect("launch", message);
}

function fetchScript(scripts:string[]) {
	connect("fetch-script", null);
}

function runScripts(id: string, scripts:string[]) {
	connect("run-script", {
		scriptPaths: scripts.map((scriptPath) => { return path.resolve(scriptPath) }),
		id: id,
	} as RunScriptMsg);
}

function ls() {
	connect("ls", null);
}

function stop(id:string) {
	const message: StopMsg = { id: id }
	connect("stop", message);
}

function follow(id:string) {
	connect("follow", {id: id});
}

function server() {
	startServer(serverCommand.show, serverCommand.chromePath);
}

function stats(id:string) {
	const message: MetricsMsg = {id};
	prepareScreenForAnimation();
	connect("metrics", message);
}

const launchCommand = program
.command("launch [scripts...]")
.description("Starts a new room", {"scripts": "Relative paths to one or more scripts to execute on the room"})
.option("--token <token> [string]", "The headless token to use with the room", "")
.option("--id <id> [string]", "The id to give the room", "default")
.option("--nohack", "The id to give the room", false)
.action(launch);

program
.command("fetch-script")
.description("fetches the script")
.action(fetchScript);


program
.command("run <id> <scripts...>")
.description("Runs script files in an already launched room", {
	"id":"The id of the room to run the scripts on",
	"scripts": "Relative paths to one or more scritps to execute on the room"
})
.action(runScripts);

program
.command("ls")
.description("Shows the ids of the currently running rooms")
.action(ls);

program
.command("stop <id>")
.description("Stops a room", {"id": "The id of the room to close"})
.action(stop);

program
.command("follow <id>")
.description(
	"Follows a room, allowing you to see it's logs and events in real time",
	{"id": "The id of the room to follow"}
)
.action(follow);

program
.command("stats <id>")
.description(
	"Shows browser stats from a room",
	{"id": "The id of the room to get stats from"}
)
.action(stats);

const serverCommand = program
.command("server")
.description("Starts the headless chromium browser and waits for cli commands.")
.option("--show", "Show the browser window (non headless mode)", false)
.option("--chrome-path <path> [string]", "A path to a chromium executable to use with puppeter.", undefined)
.action(server);

program.version("0.2.0");

program.parseAsync();
