import * as fs from 'fs';
import * as path from 'path';

export async function writeJS(name:string, body:string) {
    var buf = Buffer.from(body, 'utf8');
    await fs.promises.writeFile(path.resolve(name),buf);
}