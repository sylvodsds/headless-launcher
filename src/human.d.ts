declare module 'human-readable-id-gen' {
    namespace IdGenerator {
        function generate():string;
    }
    interface WordCollection {
        
    }
    namespace WordCollection {
        function load(path:string):WordCollection;
    }
    function generateId():string;
   
}
